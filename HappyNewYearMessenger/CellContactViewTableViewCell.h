//
//  CellContactViewTableViewCell.h
//  Snapslidy
//
//  Created by Dan Bodnar on 8/6/14.
//  Copyright (c) 2014 365plus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellContactViewTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *photo;
@property (nonatomic, weak) IBOutlet UILabel *name;
@property (nonatomic, weak) IBOutlet UIView *isSelected;

@end
