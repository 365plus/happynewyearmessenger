//
//  Utils.m
//  etoiledevenus
//
//  Created by Cipri on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Utils.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "AdSupport/AdSupport.h"

@implementation Utils

+ (Utils *)sharedUtils
{
    static Utils *utils = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
		utils = [[Utils alloc] init];
    });
	
    return utils;
}

- (void) showConnectionMessage:(NSString*)infoMessage
{
    int now = [NSDate timeIntervalSinceReferenceDate];
    if (now - lastAlertTime < 5 * 60 || connectionAlertOnScreen)
        return;

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:infoMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    connectionAlertOnScreen = YES;
    
    lastAlertTime = [NSDate timeIntervalSinceReferenceDate];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    connectionAlertOnScreen = NO;
}

+ (void) showInfo:(NSString*)infoMessage
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:infoMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

+ (void)showLoadingInView:(UIView*)view
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = [[[NSBundle mainBundle] loadNibNamed:@"HudView" owner:nil options:nil] objectAtIndex:0];
    hud.userInteractionEnabled = NO;
}

+ (NSString*)finalDateStringFromString:(NSString*)dateString
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *gmtDateTime = [dateFormatter dateFromString:dateString];
    NSTimeInterval seconds = [gmtDateTime timeIntervalSinceDate:[Application sharedApplication].serverDate];
    NSString *finalDateString;
    if (abs(seconds) > 7 * 24 * 60 * 60 * 60 || seconds > 0) //older than one week or in in the future
    {
        [dateFormatter setDateFormat:@"dd MMM"];
        finalDateString = [dateFormatter stringFromDate:gmtDateTime];
    }
    else
    {
        seconds = abs(seconds);
        if (seconds < 60 * 60) //under one hour
        {
            int minutes = seconds / 60;
            if (minutes < 2) minutes = 2;
            finalDateString = [NSString stringWithFormat:@"%d minutes ago", minutes];
        }
        else if(seconds < 24 * 60 * 60) //under one day
        {
            int hours = seconds / (60 * 60);
            finalDateString = [NSString stringWithFormat:@"%d hours ago", hours];
        }
        else //under one week
        {
            int days = seconds / (60 * 60 * 24);
            if (days == 1)
            {
                finalDateString = @"yesterday";
            }
            else if (days < 7)
            {
                finalDateString = [NSString stringWithFormat:@"%d days ago", days];
            }
            else if (days < 366)
            {
                [dateFormatter setDateFormat:@"MMM dd"];
                finalDateString = [dateFormatter stringFromDate:gmtDateTime];
            }
            else
            {
                finalDateString = @"";
            }
        }
    }
    return  finalDateString;
}

+ (CGFloat*)componentsForColor:(UIColor*)color
{
    NSInteger numComponents = CGColorGetNumberOfComponents([color CGColor]);
	const CGFloat *components = CGColorGetComponents([color CGColor]);
    
    CGFloat *k = malloc(4 * sizeof(CGFloat));
	if (numComponents == 2)
    {
		k[0] = components[0];
		k[1] = components[0];
		k[2] = components[0];
		k[3] = components[1];
	}
	else
    {
		k[0] = components[0];
		k[1] = components[1];
		k[2] = components[2];
		k[3] = components[3];
	}
    
    return k;
}

+ (NSString *)getUniqueID
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
    {
        return [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    }
    else
    {
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        NSString *uniqueString = [def objectForKey:@"uniqueString"];
        if (!uniqueString)
        {
            // Create universally unique identifier (object)
            CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
            uniqueString = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuidObject);
            CFRelease(uuidObject);
            
            [def setObject:uniqueString forKey:@"uniqueString"];
            [def synchronize];
        }
        
        return uniqueString;
    }
}

+ (int)daysRemainingOnSubscription:(NSDate*)subscriptionExpDate
{
    NSTimeInterval timeInt = [subscriptionExpDate timeIntervalSinceDate:[Application sharedApplication].serverDate];
    int days = timeInt / 60 / 60 / 24;
    if (days > 0)
        return days;
    else
        return 0;
}

// Assumes input like "#00FF00" (#RRGGBB).
+ (UIColor *)colorFromHexString:(NSString *)hexString
{
    // bypass '#' character if any
    if ([hexString rangeOfString:@"#"].location != NSNotFound)
        hexString = [hexString substringFromIndex:1];
    
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

#pragma mark - IMAGE CACHE

+ (NSString*)getImagePath:(NSString*)imageName
{
    imageName = [imageName stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    imageName = [imageName stringByReplacingOccurrencesOfString:@":" withString:@""];
    //create folder
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *folderPath = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], @"images"];
    [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];
    
    NSString *pathString = [NSString stringWithFormat:@"%@/%@", folderPath, imageName];
    return pathString;
}

+ (UIImage*)getCachedImageWithName:(NSString*)imageName
{
    if (!imageName.length)
        return nil;
    NSString *pathString = [self getImagePath:imageName];
    //download it
    if(![[NSFileManager defaultManager] fileExistsAtPath:pathString])
    {
        return nil;
    }
    else
    {
        UIImage *img = [UIImage imageWithContentsOfFile:pathString];
        return img;
    }
}

+ (BOOL)saveCachedImage:(UIImage*)image withName:(NSString*)imageName
{
    NSString *pathString = [self getImagePath:imageName];
    // Save Image
    NSData *imageData = UIImagePNGRepresentation(image);
    NSError* error;
    BOOL success = [imageData writeToFile:pathString options:NSDataWritingAtomic error:&error];
    return success;
}

+ (BOOL)urlHasSameDomain:(NSURL*)firstUrl withUrl:(NSURL*)secondUrl
{
    NSString *host1 = [firstUrl host];
    NSString *host2 = [secondUrl host];

    if ([host1 isEqualToString:host2])
    {
        return YES;
    }
    return NO;
}

@end


@implementation NSObject (PerformBlockAfterDelay)

- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay
{
    block = [block copy];
    [self performSelector:@selector(fireBlockAfterDelay:)
               withObject:block
               afterDelay:delay];
}

- (void)performAfterDelay:(NSTimeInterval)delay block:(void (^)(void))block
{
    [self performBlock:block afterDelay:delay];
}

- (void)fireBlockAfterDelay:(void (^)(void))block {
    block();
}

@end

