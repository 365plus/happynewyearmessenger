//
//  ContactsViewController.m
//  Snapslidy
//
//  Created by Dan Bodnar on 8/6/14.
//  Copyright (c) 2014 365plus. All rights reserved.
//

#import "ContactsViewController.h"
#import "CellContactViewTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@interface ContactsViewController ()

@end

@implementation ContactsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.selectedContacts = [[NSMutableArray alloc] init];
    
    self.skipButton.titleLabel.font = [UIFont fontWithName:@"fontello" size:17];
    self.sendButton.titleLabel.font = [UIFont fontWithName:@"fontello" size:17];
    
    [self.emailView setBackgroundColor:[UIColor colorWithRed:156.0f/255.0f green:156.0f/255.0f blue:156.0f/255.0f alpha:0.8f]];
    
    [self.emailPopUp.layer setCornerRadius:15];
    self.emailPopUp.clipsToBounds = YES;
    
    [self.emailContent.layer setCornerRadius:15];
    self.emailContent.clipsToBounds = YES;
    
    [[self.emailPopUp layer] setBorderWidth:2.0f];
    [[self.emailPopUp layer] setBorderColor:[UIColor colorWithRed:56.0f/255.0f green:84.0f/255.0f blue:135.0f/255.0f alpha:1.0f].CGColor];
    [self.emailPopUp setBackgroundColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1.0f]];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.emailView addGestureRecognizer:gestureRecognizer];
    

    
}

-(void)viewDidAppear:(BOOL)animated
{
    self.contacts = [self getAllContacts];
    
    [self.selectedContacts removeAllObjects];
    [self.selectedContacts addObjectsFromArray:self.contacts];
    
    [self setUpTableView];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    [super viewDidAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellContactViewTableViewCell *cell = (CellContactViewTableViewCell*)[self.contactsTable cellForRowAtIndexPath:[NSIndexPath indexPathForItem:indexPath.row inSection:0]];
    
    ContactsData *selectedContact = (ContactsData*)[self.contacts objectAtIndex:indexPath.row];
    
    if([self.selectedContacts containsObject:selectedContact])
    {
        cell.isSelected.backgroundColor = [UIColor clearColor];
        [cell.name setTextColor:[UIColor colorWithRed:156.0f/255.0f green:156.0f/255.0f blue:156.0f/255.0f alpha:1.0f]];
        [cell.photo setAlpha:0.60f];
        
        [self.selectedContacts removeObject:selectedContact];
    }
    else
    {
        cell.isSelected.backgroundColor = [UIColor colorWithRed:56.0f/255.0f
                                                          green:84.0f/255.0f
                                                           blue:135.0f/255.0f
                                                          alpha:1.0f];
        
        [cell.name setTextColor:[UIColor blackColor]];
        [cell.photo setAlpha:1.0f];
        
        [self.selectedContacts addObject:selectedContact];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contacts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellContactViewTableViewCell *cell = [self.contactsTable dequeueReusableCellWithIdentifier:@"CellContactViewTableViewCell"];
    
    if (cell == nil)
        cell = [[CellContactViewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                     reuseIdentifier:@"CellContactViewTableViewCell"];
    
    ContactsData *contact = (ContactsData*)[self.contacts objectAtIndex:indexPath.row];
    cell.name.text = [NSString stringWithFormat:@"%@ %@", contact.firstName, contact.lastName];

    cell.photo.image = contact.photo;
    cell.photo.layer.cornerRadius = 20;
    cell.photo.clipsToBounds = YES;
    
    cell.isSelected.layer.cornerRadius = 15;
    cell.isSelected.clipsToBounds = YES;
    
    [[cell.isSelected layer] setBorderWidth:1.0f];
    [[cell.isSelected layer] setBorderColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1.0f].CGColor];
    
    if(![self.selectedContacts containsObject:contact])
    {
        cell.isSelected.backgroundColor = [UIColor clearColor];
        [cell.name setTextColor:[UIColor colorWithRed:156.0f/255.0f green:156.0f/255.0f blue:156.0f/255.0f alpha:1.0f]];
        [cell.photo setAlpha:0.60f];
    }
    else
    {
        cell.isSelected.backgroundColor = [UIColor colorWithRed:56.0f/255.0f
                                                          green:84.0f/255.0f
                                                           blue:135.0f/255.0f
                                                          alpha:1.0f];
        [cell.name setTextColor:[UIColor blackColor]];
        [cell.photo setAlpha:1.0f];
    }
    return cell;
}

-(NSMutableArray *)getAllContacts
{
    CFErrorRef *error = nil;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    
    __block BOOL accessGranted = NO;
    if (ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    
    if (accessGranted) {
        
#ifdef DEBUG_MODE
        NSLog(@"Fetching contact info ----> ");
#endif
        
        [self.HUD show:YES];
        
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
        
        if(addressBook==nil)
            return nil;
        
        ABRecordRef source = ABAddressBookCopyDefaultSource(addressBook);
        NSArray *allPeople = (__bridge_transfer NSArray *)ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonSortByFirstName);
        //ABAddressBookCopyArrayOfAllPeople(addressBook);
        
        CFIndex nPeople = [allPeople count];
        NSMutableArray* items = [NSMutableArray arrayWithCapacity:nPeople];
        
        for (int i = 0; i < nPeople; i++)
        {
            ContactsData *contacts = [ContactsData new];
            
            ABRecordRef person = (__bridge ABRecordRef)allPeople[i];
            
            //get First Name and Last Name
            contacts.firstName = CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty));
            contacts.lastName =  CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty));
            
            if (!contacts.firstName)
                contacts.firstName = @"";
        
            if (!contacts.lastName)
                contacts.lastName = @"";
        
            
            //get contacts picture, if pic doesn't exists, show standart one
            NSData  *imgData = (__bridge NSData *)ABPersonCopyImageData(person);
            contacts.photo= [UIImage imageWithData:imgData];
            if (!contacts.photo) {
                contacts.photo = [UIImage imageNamed:@"user_no_frame.png"];
            }
            
            
            //get Phone Numbers
            NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
            
            ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);
            for(CFIndex i=0;i<ABMultiValueGetCount(multiPhones);i++) {
                
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                NSString *phoneNumber = (__bridge NSString *) phoneNumberRef;
                [phoneNumbers addObject:phoneNumber];
                
                //NSLog(@"All numbers %@", phoneNumbers);
            }
            
            [contacts setPhoneNumbers:phoneNumbers];
            
            
            //get Contact email
            //NSMutableArray *contactEmails = [NSMutableArray new];
            /*
            NSString *contactEmails = [NSString new];
            ABMultiValueRef multiEmails = ABRecordCopyValue(person, kABPersonEmailProperty);
            
            for (CFIndex i=0; i<ABMultiValueGetCount(multiEmails); i++) {
                CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
                NSString *contactEmail = (__bridge NSString *)contactEmailRef;
                
                //if([contactEmail rangeOfString:@"facebook.com"].location == NSNotFound)
                    contactEmails = [contactEmails length]==0 ? contactEmail : [NSString stringWithFormat:@"%@;%@", contactEmails, contactEmail];
                
            }
            
            [contacts setEmails:contactEmails];
            */
            
            if([contacts.phoneNumbers count] > 0)
                [items addObject:contacts];
            
        }
        return items;
    } else {
        
#ifdef DEBUG_MODE
        NSLog(@"Cannot fetch Contacts");
#endif
        [self.HUD hide:YES];
        [self goToSharePageMovieWithId];
        return NO;
    }
}

-(IBAction)selectAll:(id)sender
{
    [self.selectedContacts removeAllObjects];
    if([self.selectAllBtn.titleLabel.text  isEqual: @"Select all"])
    {
        [self.selectedContacts addObjectsFromArray:self.contacts];
        [self.selectAllBtn setTitle:@"Select none" forState:UIControlStateNormal];
    } else {
        [self.selectAllBtn setTitle:@"Select all" forState:UIControlStateNormal];
    }
    
    [self.contactsTable reloadData];
}

-(IBAction)goToSharePageMovieWithId
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
}

-(void) setUpTableView
{
    [self.contactsTable setDelegate:self];
    [self.contactsTable setDataSource:self];
    [self.contactsTable reloadData];
    
    [self.HUD hide:YES];
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self goToSharePageMovieWithId];
}

- (void) hideKeyboard {
    [self.emailContent resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.emailContent resignFirstResponder];
    return NO;
}

-(IBAction)sendEmail:(id)sender
{
    if([self.selectedContacts count]>0)
    {
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            controller.body = [NSString stringWithFormat:@"Happy New Year!!"];
            
            NSMutableArray *phoneNumbers = [[NSMutableArray alloc] initWithCapacity:[self.contacts count]];
            
            for(ContactsData *contact in self.selectedContacts)
                [phoneNumbers addObjectsFromArray:contact.phoneNumbers];
            
            controller.recipients = phoneNumbers;
            controller.messageComposeDelegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
    else
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"You need to select at least one person to proceed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(IBAction)seeEmail:(id)sender
{
    if([self.selectedContacts count]>0)
    {
        [self sendSMS];
    }
    else
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"You need to select at least one person to proceed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(IBAction)cancelEmail:(id)sender
{
    [self.emailContent resignFirstResponder];
    [self.emailView setHidden:YES];
}


-(IBAction)sendSMS
{
    if([self.selectedContacts count]>0)
    {
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            controller.body = self.messageToBeSent;
            
            NSMutableArray *phoneNumbers = [[NSMutableArray alloc] initWithCapacity:[self.contacts count]];
            
            for(ContactsData *contact in self.selectedContacts)
                [phoneNumbers addObjectsFromArray:contact.phoneNumbers];
            
            controller.recipients = phoneNumbers;
            controller.messageComposeDelegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
    else
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"You need to select at least one person to proceed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}


@end
