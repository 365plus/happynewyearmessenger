//
//  ContactsViewController.h
//  Snapslidy
//
//  Created by Dan Bodnar on 8/6/14.
//  Copyright (c) 2014 365plus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "ContactsData.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"

@interface ContactsViewController : UIViewController <UITextViewDelegate, UITableViewDataSource, UITableViewDelegate, MBProgressHUDDelegate, MFMessageComposeViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *contactsTable;
@property (nonatomic, weak) IBOutlet UIButton *skipButton;
@property (nonatomic, weak) IBOutlet UIButton *sendButton;
@property (nonatomic, weak) IBOutlet UIView *emailView;
@property (nonatomic, weak) IBOutlet UIView *emailPopUp;
@property (nonatomic, weak) IBOutlet UITextView *emailContent;
@property (nonatomic, weak) IBOutlet UIButton *selectAllBtn;

@property (nonatomic, strong) NSString *messageToBeSent;
@property (nonatomic, strong) NSMutableArray *contacts;
@property (nonatomic, strong) NSMutableArray *selectedContacts;
@property (nonatomic, strong) MBProgressHUD *HUD;

-(IBAction)goToSharePageMovieWithId;
-(IBAction)sendEmail:(id)sender;
-(IBAction)seeEmail:(id)sender;
-(IBAction)cancelEmail:(id)sender;
-(IBAction)selectAll:(id)sender;
@end
