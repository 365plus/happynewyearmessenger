//
//  HomeViewController.m
//  HappyNewYearMessenger
//
//  Created by Dan Bodnar on 12/13/14.
//  Copyright (c) 2014 CP. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)goToContacts:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    ContactsViewController *cntView = [storyboard instantiateViewControllerWithIdentifier:@"ContactsViewController"];
    cntView.messageToBeSent = @"Happy new year!!!";
    
    [self.navigationController showViewController:cntView sender:self];
}

@end
