//
//  Utils.h
//  etoiledevenus
//
//  Created by Cipri on 2/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "MBProgressHUD.h"
#import "Application.h"
#import "Customization.h"
#import "Version.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "UIImageView+AFNetworking.h"
#import "Config.h"

//define .....
#define top(view) (view.frame.origin.y)
#define bottom(view) (view.frame.origin.y + view.frame.size.height)
#define left(view) (view.frame.origin.x)
#define right(view) (view.frame.origin.x + view.frame.size.width)

#define expandHeight(view, diff) view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, view.frame.size.height + diff)
#define moveY(view, diff) view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y + diff, view.frame.size.width, view.frame.size.height)

#define appDelegate ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#define isIphone ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
#define IS_IPHONE ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
#define IS_IPAD (!IS_IPHONE)
#define IS_IPHONE_5 (IS_IPHONE && (fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON ))

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define CACHE_INTERVAL_MONTH 60 * 60 * 24 * 30
#define CACHE_INTERVAL_WEEK 60 * 60 * 24 * 7
#define CACHE_INTERVAL_DAY 60 * 60 * 24
#define CACHE_INTERVAL_HOUR 60 * 60
#define CACHE_INTERVAL_MINUTE 60
#define CACHE_INTERVAL_INFINITE				(60*60*24*365)

@interface Utils : NSObject <UIActionSheetDelegate, MFMailComposeViewControllerDelegate, UIAlertViewDelegate>
{
    int lastAlertTime;
    BOOL connectionAlertOnScreen;
}
+ (Utils *)sharedUtils;

+ (void) showInfo:(NSString*)infoMessage;
- (void) showConnectionMessage:(NSString*)infoMessage;
+ (void)showLoadingInView:(UIView*)view;
+ (NSString*)finalDateStringFromString:(NSString*)dateString;

+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (NSString *)getUniqueID;
+ (CGFloat*)componentsForColor:(UIColor*)color;

+ (NSString*)getImagePath:(NSString*)imageName;
+ (UIImage*)getCachedImageWithName:(NSString*)imageName;
+ (BOOL)saveCachedImage:(UIImage*)image withName:(NSString*)imageName;

+ (int)daysRemainingOnSubscription:(NSDate*)subscriptionExpDate;

@end

@interface NSObject(PerformBlockAfterDelay)

- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay;
- (void)performAfterDelay:(NSTimeInterval)delay block:(void (^)(void))block;

@end
