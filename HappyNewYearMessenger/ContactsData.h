//
//  ContactsData.h
//  Snapslidy
//
//  Created by Dan Bodnar on 8/6/14.
//  Copyright (c) 2014 365plus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ContactsData : NSObject

@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSMutableArray *phoneNumbers;
@property (nonatomic, strong) NSString *emails;
@property (nonatomic, strong) UIImage *photo;

@end
