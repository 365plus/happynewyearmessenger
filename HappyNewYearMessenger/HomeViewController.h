//
//  HomeViewController.h
//  HappyNewYearMessenger
//
//  Created by Dan Bodnar on 12/13/14.
//  Copyright (c) 2014 CP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactsViewController.h"

@interface HomeViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIButton *contactsBtn;

-(IBAction)goToContacts:(id)sender;

@end
