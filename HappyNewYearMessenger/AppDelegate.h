//
//  AppDelegate.h
//  HappyNewYearMessenger
//
//  Created by MacBook on 13/12/14.
//  Copyright (c) 2014 CP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

